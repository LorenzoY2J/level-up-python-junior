import random 


def choice_sort(lst):
	for i in range(len(lst)-1):
		min_elem = lst[i]
		min_i = i
		for j in range(i+1, len(lst)):
			if min_elem > lst[j]:
				min_elem = lst[j]
				min_i = j

		if min_i != i:
			temp = lst[i]
			lst[i] = lst[min_i]
			lst[min_i] = temp
	return lst

lst = [random.randint(0, 100) for x in range(10)]

print(lst)
print(choice_sort(lst))
