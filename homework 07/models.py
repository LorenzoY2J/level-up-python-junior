from sqlalchemy import Column, Integer, String, DateTime

from base import Base


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, autoincrement=True)
    mail = Column(String)
    password = Column(String)
    balance = Column(Integer)
    register_date = Column(DateTime)

    def __str__(self):
        return f"Пользователь с номером {self.id} с почтой {self.mail} с паролем {self.password}."
