import numpy as np
from flask import Flask, request, jsonify

# константы
API_KEY = '123321'
MATRIX_FILE = 'matrix_distance'

# загрузка матрицы расстояний
try:
    distances = np.load(open(MATRIX_FILE, "rb"))
except FileNotFoundError as e:
    raise Exception(f"Failed to load distance matrix from file: {e}")


# функция для поиска кратчайшего пути
def find_shortest_path(graph, start, end):
    """
    Реализует алгоритм Дейкстры для поиска кратчайшего пути между двумя городами.

    Аргументы:
        graph (numpy.ndarray): Матрица расстояний, представляющая расстояния между городами.
        start (int): Индекс начального города.
        end (int): Индекс конечного города.

    Возвращает:
        Кортеж, содержащий кратчайший путь в виде списка индексов городов и общее расстояние.
        Если путь не существует, возвращает (None, None).
    """
    n = graph.shape[0]
    unvisited = set(range(n))
    distance = [float('inf')] * n
    distance[start] = 0
    path = [[] for _ in range(n)]
    while unvisited:
        current = min(unvisited, key=lambda x: distance[x])
        if current == end:
            return path[end] + [end], distance[end]
        unvisited.remove(current)
        for neighbor in range(n):
            if graph[current, neighbor] and neighbor in unvisited:
                new_distance = distance[current] + graph[current, neighbor]
                if new_distance < distance[neighbor]:
                    distance[neighbor] = new_distance
                    path[neighbor] = path[current] + [current]
    return None, None


# обработчик запросов на получение кратчайшего пути
app = Flask(__name__)


@app.route('/shortest_path', methods=['GET'])
def shortest_path():
    # проверка авторизации
    api_key = request.headers.get('X-API-KEY')
    if api_key != API_KEY:
        return jsonify({'message': 'Authorization failed.'}), 401
    # получение параметров запроса
    city_start = request.args.get('city_start')
    city_finish = request.args.get('city_finish')
    # проверка наличия параметров
    if city_start is None or city_finish is None:
        return jsonify({'message': 'Missing parameters.'}), 400
    # преобразование параметров в тип int
    try:
        city_start = int(city_start)
        city_finish = int(city_finish)
    except ValueError:
        return jsonify({'message': 'Invalid parameters.'}), 400
    # поиск кратчайшего пути
    path, distance = find_shortest_path(distances, city_start, city_finish)
    if path is None:
        return jsonify({'message': 'No road.'}), 404
    # преобразование значений numpy.int64 в int
    path = [int(city) for city in path]
    distance = int(distance)
    # формирование ответа
    response = {'path': path, 'distance': distance}
    return jsonify(response)


if __name__ == '__main__':
    app.run()
