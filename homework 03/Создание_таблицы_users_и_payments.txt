1) Создание таблицы users: 

CREATE TABLE users
(
Id SERIAL PRIMARY KEY,
mail CHARACTER VARYING(50) UNIQUE,
password CHARACTER VARYING(255),
balance INTEGER,
register_date DATE
);

Создание таблицы payments: 

CREATE TABLE payments
(
user_id SERIAL PRIMARY KEY,
amount INTEGER,
date DATE,
completed BOOLEAN,
FOREIGN KEY (user_id) REFERENCES Users (Id)
);