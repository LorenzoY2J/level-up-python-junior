class LinkedList:
	head = None

	class Node:
		element = None
		next_node = None

		def __init__(self, element, next_node = None):
			self.element = element
			self.next_node = next_node

	def append(self, element):
		if not self.head:
			self.head = self.Node(element)
			return element
		node = self.head

		while node.next_node:
			node = node.next_node

		node.next_node = self.Node(element)


	def print_all_nodes(self):
		node = self.head

		while node.next_node:
			print(f"{node.element} -> ", end="")
			node = node.next_node
		print(node.element)

	def reverse_list(self):
		temp = self.head
		tail = self.Node(element=self.head.element)
		while temp.next_node:
			temp = temp.next_node
			tail = self.Node(element=temp.element, next_node = tail)
		return tail



linked_list = LinkedList()

linked_list.append(5)
linked_list.append(2)
linked_list.append(11)
linked_list.append(4)

linked_list.print_all_nodes()
linked_list.reverse_list()


