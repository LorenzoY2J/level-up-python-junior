from base_db import DbBase
from models import User


class DbUsers(DbBase):
    data_model = User


db_users = DbUsers()
