from typing import Union

from fastapi import APIRouter
from pydantic import BaseModel

from app.db.db_user import db_users

router = APIRouter(
    prefix="/api",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)


@router.get("/users/{user_id}")
def get_user(user_id: int):
    user = db_users.get({"id": user_id})
    return user

# class Item(BaseModel):
#     name: str
#     price: float
#     is_offer: Union[bool, None] = None


# @router.get("/items/{item_id}")
# def read_item(item_id: int, q: Union[str, None] = None):
#     return {"item_id": item_id, "q": q}
#
#
# @router.post("/items/{item_id}")
# def read_item2(item_id: int, item: Item):
#     return {"item_id": item_id, "name": item.name, "price": item.price, "is_offer": item.is_offer}
