class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def isBalanced(root: TreeNode) -> bool:
    # Функция для вычисления высоты дерева и проверки сбалансированности
    def height(node):
        if not node:
            return 0

        left_height = height(node.left)
        right_height = height(node.right)

        if left_height == -1 or right_height == -1 or abs(left_height - right_height) > 1:
            return -1

        return max(left_height, right_height) + 1

    return height(root) != -1


# Создаем сбалансированное дерево высотой 2
tree1 = TreeNode(1)
tree1.left = TreeNode(2)
tree1.right = TreeNode(3)

# Создаем сбалансированное дерево высотой 3
tree2 = TreeNode(1)
tree2.left = TreeNode(2)
tree2.right = TreeNode(3)
tree2.left.left = TreeNode(4)
tree2.left.right = TreeNode(5)

# Создаем несбалансированное дерево высотой 3
tree3 = TreeNode(1)
tree3.left = TreeNode(2)
tree3.left.left = TreeNode(3)
tree3.left.left.left = TreeNode(4)

print(isBalanced(tree1))
print(isBalanced(tree2))
print(isBalanced(tree3))
