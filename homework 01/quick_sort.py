import random 
import time

def quick_sort(lst):
	if len(lst) <= 1:
		return lst 
	else:
		oporn_elem = lst.pop(len(lst)//2)

	left_lst = []
	right_lst = []

	for i in lst:
		if i > oporn_elem:
			right_lst.append(i)
		else:
			left_lst.append(i)	
	return quick_sort(left_lst) + [oporn_elem] + quick_sort(right_lst)


def choice_sort(lst):
	for i in range(len(lst)-1):			
		min_elem = lst[i]
		min_i = i
		for j in range(i+1, len(lst)):
			if min_elem > lst[j]:
				min_elem = lst[j]
				min_i = j

		if min_i != i:
			temp = lst[i]
			lst[i] = lst[min_i]
			lst[min_i] = temp	
	return lst


lst = [random.randint(0, 100) for x in range(10)]
lst1 = []
lst1.extend(lst)
print(lst)
start = time.time()
print(choice_sort(lst))
print(f"Время выполнения сортировки choice_sort c 10 элементами: {time.time() - start} сек.")
print(lst1)
start = time.time()
print(quick_sort(lst1))
print(f"Время выполнения сортировки quick_sort с 10 элементами: {time.time() - start} сек.")
print("\n")

lst = [random.randint(0, 100) for x in range(1000)]
lst1 = []
lst1.extend(lst)
start = time.time()
choice_sort(lst)
print(f"Время выполнения сортировки choice_sort с 1000 элементами: {time.time() - start} сек.")
start = time.time()
quick_sort(lst1)
print(f"Время выполнения сортировки quick_sort с 1000 элементами: {time.time() - start} сек.")
print("\n")

lst = [random.randint(0, 100) for x in range(10000)]
lst1 = []
lst1.extend(lst)
start = time.time()
choice_sort(lst)
print(f"Время выполнения сортировки choice_sort с 10000 элементами: {time.time() - start} сек.")
start = time.time()
quick_sort(lst1)
print(f"Время выполнения сортировки quick_sort с 10000 элементами: {time.time() - start} сек.")

lst = [random.randint(0, 100) for x in range(100000)]
lst1 = []
lst1.extend(lst)
start = time.time()
choice_sort(lst)
print(f"Время выполнения сортировки choice_sort с 100000 элементами: {time.time() - start} сек.")
start = time.time()
quick_sort(lst1)
print(f"Время выполнения сортировки quick_sort с 100000 элементами: {time.time() - start} сек.")
