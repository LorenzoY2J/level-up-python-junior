from pydantic import BaseSettings


class DatabaseConfig(BaseSettings):
    login: str = "postgres"
    password: str = "3752"
    host: str = "localhost"
    port: str = "5432"
    database: str = "homework"


config = DatabaseConfig()
