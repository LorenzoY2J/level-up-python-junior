from datetime import datetime
from typing import Optional

from base import Session
from models import User


def add_user(id: int, mail: str, password: str, balance: int, register_date: datetime) -> None:
    user = User(id=id, mail=mail, password=password, balance=balance, register_date=register_date)
    session = Session()
    session.add(user)
    session.commit()
    session.close()


def get_user(id: int) -> Optional[User]:
    session = Session()
    user = session.query(User).filter_by(id=id).first()
    session.close()
    return user


def delete_user(id: int):
    session = Session()
    user = session.query(User).filter_by(id=id).first()
    session.delete(user)
    session.commit()
    session.close()


def change_password(id: int, new_password: str) -> User:
    session = Session()
    user = session.query(User).filter_by(id=id).first()
    user.password = new_password
    session.add(user)
    session.commit()
    session.close()
    return get_user(id)


add_user(1, "test1", "qwerty1", 1000, datetime.now())
add_user(2, "test2", "qwerty2", 2000, datetime.now())
add_user(3, "test3", "qwerty3", 3000, datetime.now())
add_user(4, "test4", "qwerty4", 4000, datetime.now())

u = get_user(1)
print(u)

change_password(1, "123456")
u = get_user(1)
print(u)
